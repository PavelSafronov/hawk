GIT_COMMIT := $(shell git rev-parse --short HEAD)
BUILD_TIME := $(shell date -u '+%Y-%m-%d %H:%M:%S')

all: build

build:
	echo "var hawkInfo = {version: '${GIT_COMMIT}', updated: '${BUILD_TIME}'};" > public/gen/version.js
