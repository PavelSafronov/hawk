const apiExpiry = 3600 * 1000; // 1 hour

function createCallbackState(processedCount, totalCount) {
    return {
        processedCount: processedCount,
        totalCount: totalCount
    };
}

async function getAlbums(statusCallback) {
    const db = await openDb();
    const cacheKey = 'albums';
    const cachedAlbums = await getWithExpiry(db, cacheKey);
    if (cachedAlbums) {
        if (statusCallback) {
            statusCallback(createCallbackState(cachedAlbums.length, cachedAlbums.length))
        }
        return cachedAlbums;
    }

    const accessToken = await getAccessToken();
    const albums = [];
    let url = 'https://api.spotify.com/v1/me/albums?limit=50';
    while (url) {
        const response = await fetch(url, {
            headers: {
                Authorization: 'Bearer ' + accessToken
            }
        });
        const data = await response.json();
        albums.push(...data.items);
        if (statusCallback) {
            statusCallback(createCallbackState(albums.length, data.total))
        }
        url = data.next;
    }

    await setWithExpiry(db, cacheKey, albums, apiExpiry);
    return albums;
}

async function batchGetTracks(albumIds, statusCallback) {
    const batchSize = 20
    if (albumIds.length == 0) {
        return [];
    }
    if (albumIds.length > batchSize) {
        const allTracks = []
        while (albumIds.length > 0) {
            const batch = albumIds.splice(0, batchSize);
            albumIds = albumIds.splice(batchSize);
            const tracks = await batchGetTracks(batch, statusCallback);
            allTracks.push(...tracks);
        }
        return allTracks;
    }

    const db = await openDb();
    const cacheKey = `tracks-${albumIds.join('-')}`;
    const cachedTracks = await getWithExpiry(db, cacheKey);
    if (cachedTracks) {
        if (statusCallback) {
            statusCallback(createCallbackState(cachedTracks.length, cachedTracks.length))
        }
        return cachedTracks;
    }

    const tracks = [];
    const accessToken = await getAccessToken();
    let url = `https://api.spotify.com/v1/albums?ids=${albumIds.join(',')}&market=from_token`;
    while (url) {
        const response = await fetch(url, {
            headers: {
                Authorization: 'Bearer ' + accessToken
            }
        });
        const data = await response.json();
        data.albums.forEach(album => {
            // combine the album data with the track data
            tracks.push(...album.tracks.items.map(track => {
                track.album = {
                    id: album.id,
                    name: album.name,
                    images: album.images,
                    external_urls: album.external_urls
                }
                return track;
            }));
        });
        if (statusCallback) {
            statusCallback(createCallbackState(tracks.length, tracks.length))
        }
        url = data.next;
    }

    await setWithExpiry(db, cacheKey, tracks, apiExpiry);
    return tracks;
}

async function getTracks(albumId, statusCallback) {
    const db = await openDb();
    const cacheKey = `tracks-${albumId}`;
    const cachedTracks = await getWithExpiry(db, cacheKey);
    if (cachedTracks) {
        if (statusCallback) {
            statusCallback(createCallbackState(cachedTracks.length, cachedTracks.length))
        }
        return cachedTracks;
    }

    const tracks = [];
    const accessToken = await getAccessToken();
    let url = `https://api.spotify.com/v1/albums/${albumId}/tracks?limit=50`;
    while (url) {
        const response = await fetch(url, {
            headers: {
                Authorization: 'Bearer ' + accessToken
            }
        });
        const data = await response.json();
        tracks.push(...data.items);
        if (statusCallback) {
            statusCallback(createCallbackState(tracks.length, data.total))
        }
        url = data.next;
    }

    await setWithExpiry(db, cacheKey, tracks, apiExpiry);
    return tracks;
}

async function playTrack(trackId) {
    let accessToken = await getAccessToken();
    console.info(`Playing track ${trackId}`);
    const response = await fetch(`https://api.spotify.com/v1/me/player/play`, {
        method: 'PUT',
        headers: {
            Authorization: 'Bearer ' + accessToken
        },
        body: JSON.stringify({
            uris: [`spotify:track:${trackId}`]
        })
    });
    if (response.status != 204) {
        const json = await response.json();
        throw new Error(`Failed to play track ${trackId}: ${response.status}\n${JSON.stringify(json)}`);
    }
}

async function getPlayingTrack() {
    const accessToken = await getAccessToken();
    const response = await fetch("https://api.spotify.com/v1/me/player", {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    });

    if (response.status == 204) {
        return null;
    }

    const data = await response.json();
    return data;
}

async function play() {
    const accessToken = await getAccessToken();
    await fetch("https://api.spotify.com/v1/me/player/play", {
        method: "PUT",
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    });
}

async function pause() {
    const accessToken = await getAccessToken();
    await fetch("https://api.spotify.com/v1/me/player/pause", {
        method: "PUT",
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    });
}

async function getAvailableDevices() {
    const accessToken = await getAccessToken();
    const response = await fetch("https://api.spotify.com/v1/me/player/devices", {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    });
    const data = await response.json();
    return data.devices;
}

async function transferPlayback(deviceId) {
    const accessToken = await getAccessToken();
    const response = await fetch("https://api.spotify.com/v1/me/player", {
        method: "PUT",
        headers: {
            Authorization: 'Bearer ' + accessToken
        },
        body: JSON.stringify({
            device_ids: [deviceId]
        })
    });
    if (response.status != 204) {
        const json = await response.json();
        throw new Error(`Failed to transfer playback to ${deviceId}: ${response.status}\n${JSON.stringify(json)}`);
    }
}