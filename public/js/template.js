/**
 * Get a template from a string
 * https://stackoverflow.com/a/41015840
 * @param  {String} str    The string to interpolate
 * @param  {Object} params The parameters
 * @return {String}        The interpolated string
 */
function interpolate(str, params) {
    let names = Object.keys(params);
    let vals = Object.values(params);
    return new Function(...names, `return \`${str}\`;`)(...vals);
}

function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

function createTemplatedHtml(templateId, params) {
    const template = document.querySelector(`#${templateId}`);
    const html = interpolate(template.innerHTML, params);
    return html;
}