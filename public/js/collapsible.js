var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
    const item = coll[i];
    if (item.classList.contains("active")) {
        const content = item.nextElementSibling;
        content.style.maxHeight = content.scrollHeight + "px";
        item.after = "▼"
    }
    item.addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
            item.after = "▶"
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
            item.after = "▼"
        }
    });
}