async function openDb() {
    const promise = new Promise((resolve, reject) => {
        const openRequest = indexedDB.open("hawkDb", 1);

        openRequest.onupgradeneeded = function (e) {
            const db = e.target.result;
            if (!db.objectStoreNames.contains("spotify")) {
                db.createObjectStore("spotify");
            }
        };

        openRequest.onsuccess = function (e) {
            const db = e.target.result;
            resolve(db);
        };

        openRequest.onerror = function (e) {
            console.error("Error opening IndexedDB:", e);
            reject(e);
        };
    });
    const db = await promise;
    return db;
}

async function deleteDb() {
    const promise = new Promise((resolve, reject) => {
        const deleteRequest = indexedDB.deleteDatabase("hawkDb");

        deleteRequest.onsuccess = function (e) {
            resolve();
        };

        deleteRequest.onerror = function (e) {
            console.error("Error deleting IndexedDB:", e);
            reject(e);
        };
    });
    await promise;
}

async function setWithExpiry(db, key, value, ttl) {
    const item = {
        value: value,
        expiry: Date.now() + ttl
    };
    const transaction = db.transaction("spotify", "readwrite");
    const store = transaction.objectStore("spotify");
    store.put(item, key);
    await transaction.complete;
}

async function getWithExpiry(db, key) {
    const transaction = db.transaction("spotify", "readonly");
    const store = transaction.objectStore("spotify");
    const promise = new Promise((resolve, reject) => {
        const request = store.get(key);
        request.onsuccess = function (e) {
            resolve(e.target.result);
        };
        request.onerror = function (e) {
            console.error("Error getting from IndexedDB:", e);
            reject(e);
        };
    });
    const item = await promise;
    if (!item) {
        return null;
    }
    console.debug(`Cache hit for ${key}`);
    console.debug(JSON.stringify(item))
    console.debug(item)

    if (!item) {
        return null;
    }
    if (Date.now() > item.expiry) {
        return null;
    }
    return item.value;
}