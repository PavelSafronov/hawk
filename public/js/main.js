// set favicon based on environment
const isLocal = window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1";
if (isLocal) {
    document.querySelector("link[rel='icon']").href = "images/favicon-local.png";
}

// set version and updated info
const params = {
    version: hawkInfo.version,
    updated: hawkInfo.updated
};
document.getElementById("info").innerHTML = createTemplatedHtml('app-info', params)

// handle redirect from Spotify
const urlParams = new URLSearchParams(window.location.search);
const code = urlParams.get('code');
const error = urlParams.get('error');
if (code) {
    getToken(code).then(() => {
        window.history.replaceState({}, document.title, '/');
        window.location = getSpotifyRedirectURL();
    })
} else if (error) {
    console.error(error);
    window.history.replaceState({}, document.title, '/');
    window.location = getSpotifyRedirectURL();
}

// determine if the user is signed in or not
// hide and show appropriate elements, based on class names
const accessToken = localStorage.getItem('access_token');
const userIsSignedIn = !accessToken
if (userIsSignedIn) {
    hideOrShow('signedin', false);
    hideOrShow('signedout', true);
} else {
    hideOrShow('signedin', true);
    hideOrShow('signedout', false);
    setCurrentPlayingSong();
    setInterval(setCurrentPlayingSong, 5 * 1000);
}

// periodically lookup the active device
configureDevicesList();

// configure times
configureTimesList();