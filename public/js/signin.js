const clientId = '6c4c04a3751b4e409ddad7d71aad9949';
const scope = 'user-library-read user-modify-playback-state user-read-playback-state';
const prematureRefreshSeconds = 60 * 5; // 5 minutes before expiration

function getSpotifyRedirectURL() {
    // If the current hostname is 'localhost'
    if (window.location.hostname === 'localhost') {
        return 'http://localhost:5500/public/index.html';
    } else if (window.location.hostname === '127.0.0.1') {
        return 'http://127.0.0.1:5500/public/index.html';
    } else {
        return 'https://pavelsafronov.gitlab.io/hawk/index.html';
    }
}

async function signOut() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('expires_at');
    await deleteDb();
    window.location = getSpotifyRedirectURL();
}

async function signIn() {
    let codeVerifier = generateRandomString(128);
    const codeChallenge = await generateCodeChallenge(codeVerifier)
    let state = generateRandomString(16);

    localStorage.setItem('code_verifier', codeVerifier);

    const redirectUrl = getSpotifyRedirectURL();
    let args = new URLSearchParams({
        response_type: 'code',
        client_id: clientId,
        scope: scope,
        redirect_uri: redirectUrl,
        state: state,
        code_challenge_method: 'S256',
        code_challenge: codeChallenge
    });

    window.location = 'https://accounts.spotify.com/authorize?' + args;
}

async function getToken(code) {
    let codeVerifier = localStorage.getItem('code_verifier');

    const redirectUrl = getSpotifyRedirectURL();
    let body = new URLSearchParams({
        grant_type: 'authorization_code',
        code: code,
        redirect_uri: redirectUrl,
        client_id: clientId,
        code_verifier: codeVerifier
    });

    try {
        const response = await fetch('https://accounts.spotify.com/api/token', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: body
        });
        if (!response.ok) {
            console.error('HTTP status ' + response.status);
            throw new Error('HTTP status ' + response.status);
        }
        const data = await response.json();
        const expiresInSeconds = data.expires_in;
        localStorage.setItem('access_token', data.access_token);
        localStorage.setItem('refresh_token', data.refresh_token);
        localStorage.setItem('expires_at', Date.now() + expiresInSeconds * 1000);
    } catch (error) {
        console.error('Error:', error);
    }
}

function generateRandomString(length) {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

async function generateCodeChallenge(codeVerifier) {
    function base64encode(string) {
        return btoa(String.fromCharCode.apply(null, new Uint8Array(string)))
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=+$/, '');
    }

    const encoder = new TextEncoder();
    const data = encoder.encode(codeVerifier);
    const digest = await window.crypto.subtle.digest('SHA-256', data);

    return base64encode(digest);
}

async function getAccessToken() {
    let accessToken = localStorage.getItem('access_token');
    if (!accessToken) {
        throw 'No access token found';
    }

    const expiresAtText = localStorage.getItem('expires_at');
    if (!expiresAtText) {
        throw 'No expiration found';
    }
    const expiresAt = parseInt(expiresAtText);
    const shouldRefresh = Date.now() > expiresAt - prematureRefreshSeconds * 1000;
    if (shouldRefresh) {
        console.log('Refreshing access token');
        accessToken = await refreshToken();
    }

    return accessToken;
}

async function refreshToken() {
    const refreshToken = localStorage.getItem('refresh_token');
    if (!refreshToken) {
        throw 'No refresh token found';
    }

    let body = new URLSearchParams({
        grant_type: 'refresh_token',
        refresh_token: refreshToken,
        client_id: clientId
    });

    const response = await fetch('https://accounts.spotify.com/api/token', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: body,
    });
    if (!response.ok) {
        console.error('HTTP status ' + response.status);
        throw new Error('HTTP status ' + response.status);
    }
    const data = await response.json();
    const expiresInSeconds = data.expires_in;
    localStorage.setItem('access_token', data.access_token);
    localStorage.setItem('refresh_token', data.refresh_token);
    localStorage.setItem('expires_at', Date.now() + expiresInSeconds * 1000);
    return data.access_token;
}