let pageStatus = {
    text: '',
    isError: false
};
setInterval(() => {
    const statusDiv = document.getElementById('status');
    statusDiv.innerText = pageStatus.text;
    if (pageStatus.isError) {
        statusDiv.classList.add('error');
    } else {
        statusDiv.classList.remove('error');
    }
}, 100);

function reportStatus(text, isError = false) {
    pageStatus.text = text;
    pageStatus.isError = isError;
}

async function pauseOrPlay() {
    const currentTrack = await getPlayingTrack();
    if (currentTrack && currentTrack.is_playing) {
        await pause();
    } else {
        await play();
    }
    setCurrentPlayingSong();
}

async function playChosenSong(trackName, trackId) {
    // clear status
    reportStatus('')

    // get select element
    const devicesList = document.getElementById('devices');
    const deviceId = devicesList.value;
    if (!deviceId) {
        reportStatus('Cannot play track, no device selected', true);
        return;
    }

    try {
        await playTrack(trackId);
    } catch (e) {
        console.log("Error playing track")
        console.log(e);
        reportStatus(e);
    }
    setCurrentPlayingSong();
}

function setTime(minutes, seconds) {
    document.getElementById('minutes-input').value = minutes;
    document.getElementById('seconds-input').value = seconds;
}

async function searchForMatch() {
    const searchButton = document.getElementById('search');
    searchButton.setAttribute('disabled', 'disabled');
    try {
        const minutes = document.getElementById('minutes-input').value;
        const seconds = document.getElementById('seconds-input').value;
        const tracks = await findMatchingTracks(minutes, seconds);
        const resultsDiv = document.getElementById('results');
        if (tracks.length == 0) {
            resultsDiv.innerHTML = 'No songs found for that duration.';
        } else {
            resultsDiv.innerHTML = '';
            tracks.forEach(track => {
                const album = track.album;
                const images = album.images;
                let largestImage = images[0];
                const albumArtUrl = largestImage.url;
                const params = {
                    name: track.name,
                    album: track.album.name,
                    artist: track.artists[0].name,
                    duration: formatDuration(track.duration_ms),
                    albumArtUrl: albumArtUrl,
                    songUrl: track.external_urls.spotify,
                    albumUrl: album.external_urls.spotify,
                    artistUrl: track.artists[0].external_urls.spotify,
                    trackId: track.id,
                    trackHref: track.external_urls.spotify
                };

                const html = createTemplatedHtml('found-song-item', params);
                resultsDiv.innerHTML += html;
            });
        }
    } catch (e) {
        console.error(e);
        reportStatus(`Encountered error while searching: ${e}`);
    } finally {
        searchButton.removeAttribute('disabled');
    }
}

async function findMatchingTracks(minutes, seconds) {
    reportStatus(`Retrieving albums`);
    const albums = await getAlbums((state) => {
        const { processedCount, totalCount } = state;
        if (totalCount <= 0) {
            totalCount = "???"
        }
        reportStatus(`Retrieving albums: ${processedCount} of ${totalCount}`);
    });

    const checkTrack = function (track) {
        const duration = track.duration_ms;
        const durationInSeconds = Math.floor(duration / 1000);
        const durationInMinutes = Math.floor(durationInSeconds / 60);
        const durationInRemainingSeconds = durationInSeconds % 60;

        const isMatching = (durationInMinutes == minutes && durationInRemainingSeconds == seconds);
        return isMatching;
    }

    const tracks = await batchGetTracks(albums.map(album => album.album.id), null)
    const foundTracks = tracks.filter(track => checkTrack(track));

    reportStatus(`Found ${foundTracks.length} track(s)`);
    return foundTracks;
}

async function clearDb() {
    await deleteDb();
    window.location.reload();
}

const hideOrShow = function (elementClass, show) {
    const elements = document.getElementsByClassName(elementClass);
    for (let i = 0; i < elements.length; i++) {
        const element = elements[i];
        if (show) {
            element.setAttribute('style', 'display: block');
        } else {
            element.setAttribute('style', 'display: none');
        }
    }
}

function formatDuration(ms) {
    // Convert milliseconds to total seconds
    let totalSeconds = Math.floor(ms / 1000);

    // Calculate minutes and seconds
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;

    // Ensure seconds are always 2 digits
    let formattedSeconds = seconds.toString().padStart(2, '0');

    // Return formatted string
    return `${minutes}:${formattedSeconds}`;
}

async function setCurrentPlayingSong() {
    const currentSongSpan = document.getElementById('current-song');
    const currentTrack = await getPlayingTrack();
    let html;
    if (!currentTrack) {
        html = createTemplatedHtml('no-song-playing', {})
    } else {
        if (!currentTrack || !currentTrack.item) {
            return;
        }
        const album = currentTrack.item.album;
        const item = currentTrack.item;
        const name = item.name;
        const progressPercent = currentTrack.progress_ms / item.duration_ms * 100;
        const images = album.images;
        let largestImage = images[0];
        const albumArtUrl = largestImage.url;
        const songUrl = item.external_urls.spotify;
        const progress = formatDuration(currentTrack.progress_ms);
        const totalDuration = formatDuration(item.duration_ms);
        const params = {
            name: name,
            artist: album.artists[0].name,
            album: album.name,
            albumArtUrl: albumArtUrl,
            songUrl: songUrl,
            progressPercent: progressPercent,
            progress: progress,
            duration: totalDuration,
            songUrl: item.external_urls.spotify,
            albumUrl: album.external_urls.spotify,
            artistUrl: album.artists[0].external_urls.spotify,
        };

        html = createTemplatedHtml('playing-song', params)
    }
    currentSongSpan.innerHTML = html;
}

async function configureDevicesList() {
    // get select element
    const devicesList = document.getElementById('devices');

    await updateDevicesList(devicesList);
    setInterval(async () => {
        await updateDevicesList(devicesList);
    }, 1000 * 5);
    devicesList.addEventListener('change', async () => {
        const deviceId = devicesList.value;
        saveDeviceId(deviceId);
        if (deviceId) {
            console.info(`Setting device to ${deviceId}`);
            await transferPlayback(deviceId);
        }
    });
}

async function updateDevicesList(devicesList) {
    // get available devices
    const devices = await getAvailableDevices();

    // add all devices to the select as options
    let activeDeviceId = "";
    for (let i = 0; i < devices.length; i++) {
        const device = devices[i];
        const ignoreDevice = device.is_restricted || device.is_private_session;

        // if the device is active, save its id
        if (device.is_active) {
            activeDeviceId = device.id;
        }

        // check if the device is already in the list
        const existingOption = devicesList.querySelector(`option[value="${device.id}"]`);
        if (existingOption) {
            if (ignoreDevice) {
                // remove the device from the list
                devicesList.removeChild(existingOption);
            } else {
                // update the name of the device, if it has changed
                if (existingOption.text != device.name) {
                    existingOption.text = device.name;
                }
                // enable the option, if it was disabled
                if (existingOption.hasAttribute('disabled')) {
                    existingOption.removeAttribute('disabled');
                }
            }
        } else if (!ignoreDevice) {
            // add the device to the list
            const option = document.createElement('option');
            option.value = device.id;
            option.text = device.name;
            devicesList.appendChild(option);
        }
    }

    // disable options that are no longer available
    const options = devicesList.querySelectorAll('option');
    for (let i = 0; i < options.length; i++) {
        const option = options[i];
        const existingDevice = devices.find(device => device.id == option.value);
        if (!existingDevice) {
            option.setAttribute('disabled', 'disabled');
            if (option.value == devicesList.value) {
                devicesList.value = "";
                devicesList.dispatchEvent(new Event('change'));
            }
        }
    }

    // if there is an active device, select it and save the id
    if (activeDeviceId) {
        devicesList.value = activeDeviceId;
        saveDeviceId(activeDeviceId);
    } else {
        const savedDeviceId = getSavedDeviceId();
        if (savedDeviceId) {
            devicesList.value = savedDeviceId;
        }
    }
}

function saveDeviceId(deviceId) {
    localStorage.setItem('active_device_id', deviceId);
}
function getSavedDeviceId() {
    return localStorage.getItem('active_device_id');
}

function configureTimesList() {
    timesUl = document.getElementById('times');
    const createLi = (minutes, seconds) => {
        const li = document.createElement('li');
        const a = document.createElement('a');
        a.href = '#';
        a.onclick = () => {
            setTime(minutes, seconds);
        };
        a.innerText = `${minutes}:${seconds.toFixed(2).toString().padStart(5, '0').slice(0, 2)}`;
        li.appendChild(a);
        return li;
    }
    timesUl.appendChild(createLi(0, 30));
    timesUl.appendChild(createLi(1, 0));
    timesUl.appendChild(createLi(2, 0));
    timesUl.appendChild(createLi(3, 0));
    timesUl.appendChild(createLi(4, 0));
    timesUl.appendChild(createLi(5, 0));
}