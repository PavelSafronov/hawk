# Hawk

## Description

You can specify a duration (for example, "5:32"), and *Hawk* will find a song in your Spotify library with that length, and with a single click you can start playing that song.

The name *Hawk* comes from the movie [Hudson Hawk](https://www.imdb.com/title/tt0102070), where [Bruce Willis sings songs of particular lengths in order to time things](https://www.youtube.com/watch?v=I6WXVqg48Qs).

Yeah, it's weird, but I was also wondering what it would take to make this. This project ended up taking just two days, and the core functionality was working in the first few hours. Check out the git history to see how this thing evolved.

I'm pretty happy with the result, but it definitely needs a designer's touch. Pull requests are welcome!

## Links

Repo: https://gitlab.com/PavelSafronov/hawk

Pages location: https://pavelsafronov.gitlab.io/hawk/

## TODOs

1. Add a histogram of song lengths and a way to get a slice of songs from it.
